package com.dgss.user.management.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "dgss_address")
public class Address implements Serializable {
  private static final long serialVersionUID = -3542724383251833367L;
  @Id
  @GeneratedValue
  @Column(name = "id")
  private Long addressId;
  @Column(name = "lane")
  private String lane;
  @Column(name = "street")
  private String street;
  @Column(name = "area")
  private String area;
  @Column(name = "city")
  private String city;
  @Column(name = "state")
  private String state;
  @Column(name = "country")
  private String country;
  @Column(name = "pin_code")
  private String pinCode;
}
