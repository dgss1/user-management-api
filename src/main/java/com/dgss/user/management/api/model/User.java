package com.dgss.user.management.api.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "dgss_user")
public class User implements Serializable{
  private static final long serialVersionUID = -7945178689806444846L;
  @Id
  @GeneratedValue
  @Column(name = "id")
  private Integer userId;
  @Column(name = "user_name")
  private String userName;
  @Column(name = "active_from")
  private LocalDateTime activeFrom;
  @Column(name = "active_to")
  private LocalDateTime activeTo;
  @Column(name="status")
  private Boolean status;
}
