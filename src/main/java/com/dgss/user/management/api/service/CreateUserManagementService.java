package com.dgss.user.management.api.service;

import com.dgss.user.management.api.model.User;
import com.dgss.user.management.api.model.UserDto;

public interface CreateUserManagementService {
	public User createUser(UserDto userDto);
}
