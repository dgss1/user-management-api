package com.dgss.user.management.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class UserManagementApiApplication {
  public static void main(String[] args) {
    System.setProperty("spring.config.name", "user-management-api");
    System.setProperty("spring.config.location",
        "file:/var/dgss/services-config/user-management-api/");
    SpringApplication.run(UserManagementApiApplication.class, args);
  }

}
