package com.dgss.user.management.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dgss.user.management.api.model.User;
import com.dgss.user.management.api.model.UserDto;
import com.dgss.user.management.api.service.CreateUserManagementService;

@RestController
@RequestMapping("user")
public class UserManagementController {
	
	@Autowired
	private CreateUserManagementService userManagementService;
	
	@PostMapping
	public ResponseEntity<User> create(@RequestBody UserDto userDto, HttpRequest request){
		User user = userManagementService.createUser(userDto);
		return new ResponseEntity<>(user, HttpStatus.CREATED);
	}
}
